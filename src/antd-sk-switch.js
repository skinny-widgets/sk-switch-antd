
import { SkSwitchImpl } from '../../sk-switch/src/impl/sk-switch-impl.js';
import { SW_FORWARDED_ATTRS } from "../../sk-switch/src/impl/sk-switch-impl.js";

export class AntdSkSwitch extends SkSwitchImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'switch';
    }

    get buttonEl() {
        return this.comp.el.querySelector('button');
    }

    set buttonEl(el) {
        this._buttonEl = el;
    }

    get subEls() {
        return [ 'buttonEl' ];
    }

    onClick(event) {
        if (this.comp.getAttribute('checked')) {
            this.uncheck();
        } else {
            this.check();
        }
        if (! this.comp.getAttribute('checked')) {
            this.comp.setAttribute('checked', 'checked');
        } else {
            this.comp.removeAttribute('checked');
        }
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.showInvalid();
        }
    }

    forwardAttributes() {
        if (this.buttonEl) {
            for (let attrName of Object.keys(SW_FORWARDED_ATTRS)) {
                let value = this.comp.getAttribute(attrName);
                if (value) {
                    this.buttonEl.setAttribute(attrName, value);
                } else {
                    if (this.comp.hasAttribute(attrName)) {
                        this.buttonEl.setAttribute(attrName, '');
                    }
                }
                if (attrName === 'checked' && value) {
                    this.check();
                }
            }
        }
    }

    check() {
        this.buttonEl.classList.add('ant-switch-checked');
        this.buttonEl.setAttribute('aria-checked', 'true');
    }

    uncheck() {
        this.buttonEl.classList.remove('ant-switch-checked');
        this.buttonEl.setAttribute('aria-checked', 'false');
    }

    bindEvents() {
        super.bindEvents();
        this.comp.onclick = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onClick(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
    }

    unbindEvents() {
        super.unbindEvents();
        this.comp.onclick = null;
    }

    afterRendered() {
        super.afterRendered();
        this.comp.inputEl = this.buttonEl;
        this.mountStyles();
    }

    enable() {
        super.enable();
        this.buttonEl.removeAttribute('disabled');
        this.buttonEl.classList.remove('ant-switch-disabled');
    }

    disable() {
        super.disable();
        this.buttonEl.setAttribute('disabled', 'disabled');
        this.buttonEl.classList.add('ant-switch-disabled');
    }


    showInvalid() {
        this.buttonEl.classList.add('form-invalid');
    }

    showValid() {
        this.buttonEl.classList.remove('form-invalid');
    }
}
